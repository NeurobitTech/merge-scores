# Merge-Scores Python Script

## Script to merge manual adjustment in scores done in VitaScore into existing wonambi scores    

Keep your original Wonambi scores and manual QCed scores in the same directory. Make sure you stick to this naming convention:  

**Wonambi Scores:** filename.wonambi.xml  
**Manual QC Scores:** filename_ManualAdjustment.txt  

then run this script on the terminal:

```sh
python merge-scores.py 
```
You have to give the path to the folder where the files are kept. 
A new score will be added to the Wonambi file under a new rater called NEO_QC. You can access this rater inside Wonambi by going to Annotations->Rater

Dependencies:    

```sh
pip install click
```

Support: contact@neurobit.io

All rights reserved (c) 2020, Neurobit Technologies Pte Ltd  
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.