#!/usr/bin/python
# Script to merge manual scores from VitaScore into existing wonambi scores
# Keep your original Wonambi scores and manual QCed scores in the same directory
# Make sure you stick to this naming convention:
# Wonambi Scores: filename.wonambi.xml
# Manual QC Scores: filename_ManualAdjustment.txt
# then run this script on the terminal:
# 
# python merge-scores.py /path/to/directory/with/scores
#
# A new score will be added to the Wonambi file under a new rater called NEO_QC
# you can access this rater inside Wonambi by going to Annotations->Rater
#
# All rights reserved (c) 2020, Neurobit Technologies Pte Ltd
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# requires the click package
# pip install click

import os.path
import numpy as np
from glob import glob
from datetime import datetime
import xml.etree.cElementTree as ET

valid_stages = {
    255: 'Unknown',
    0: 'Wake',
    1: 'NREM1',
    2: 'NREM2',
    3: 'NREM3',
    5: 'REM',   
}

def merge_scores(filename):
    print('Processing: ' + filename)
    adjustment = filename[:-12] + '_ManualAdjustment.txt'
    
    if not os.path.isfile(adjustment):
        print('Adjustment file not found.') 
        return

    new_scores = np.genfromtxt(adjustment, dtype=np.int)
    epochs = int(new_scores.size/3)
    
    tree = ET.parse(filename)
    root = tree.getroot()

    for rater in root.findall('.//rater'):
        attrib = rater.attrib
        if 'name' in attrib:
            if attrib['name'] == 'NEO_QC':
                print('Found existing QCed scores.')
                root.remove(rater)


    rater = ET.SubElement(root,"rater")
    rater.attrib['created'] = datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")
    rater.attrib['modified'] = datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")
    rater.attrib['name'] = "NEO_QC" 
    # Add annotations
    ET.SubElement(rater,"bookmarks")
    eventRoot = ET.SubElement(rater,"events")
    # Sleep scores
    stageRoot = ET.SubElement(rater,"stages")

    for i in range(epochs):
        epoch = ET.SubElement(stageRoot,"epoch")
        start = ET.SubElement(epoch,"epoch_start")
        start.text =  str(i*30)
        stage = int(new_scores[i*3])
        end = ET.SubElement(epoch,"epoch_end")
        end.text = str(i*30 + 30)
        stage = ET.SubElement(epoch,"stage")
        stage.text = valid_stages[new_scores[i*3]]
        quality = ET.SubElement(epoch,"quality")
        quality.text = "Good"

    ET.SubElement(rater,"cycles")

    tree = ET.ElementTree(root)
    print('Updating scores')
    tree.write(filename, encoding="UTF-8")


def merge(path):
    '''
    Provide the directory path where wonambi scores and manually adjusted scores are kept.
    Script assumes this naming convention:
    Wonambi Scores: filename.wonambi.xml and Manual Scores: filename_ManualAdjustment.txt
    Manual scores will be merged into the wonambi file under a new rater named:
    NEO_QC

    '''
    files = glob(os.path.join(path,'*.wonambi.xml'))
    if len(files) == 0:
        print('No wonambi file found!')

    for f in files:
        try:
            merge_scores(f)
        except:
            print('There was a problem analysing: %s' %(f))


if __name__ == '__main__':
    path = input("Enter the path where original and modified scores are kept: ")
    if os.path.isdir(path):
        merge(path)
    else:
        print('path is not a valid directory. Cannot continue.')